package shai.front.proxy;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;

public class PostRouteLogger extends ZuulFilter {

	private static Logger log = LoggerFactory.getLogger(PostRouteLogger.class);

	@Override
	public String filterType() {
		return "post";
	}

	@Override
	public int filterOrder() {
		return 1;
	}

	@Override
	public boolean shouldFilter() {
		return true;
	}

	@Override
	public Object run() {
		RequestContext ctx = RequestContext.getCurrentContext();
		HttpServletRequest request = ctx.getRequest();

		log.info(String.format("%s request %s is routed to %s/**", request.getMethod(),
				request.getRequestURL().toString(), ctx.getRouteHost().toString()));

		return null;
	}

}