package shai.service2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class Service2Application {

	public static void main(String[] args) {
		SpringApplication.run(Service2Application.class, args);
	}

	@RequestMapping(value = "/another/available")
	public String available() {
		return "Spring in Action 2";
	}

	@RequestMapping(value = "/another/checked-out")
	public String checkedOut() {
		return "Spring Boot in Action 2";
	}
}